-- Generic function that creates classes and instantiates them to an inherited class
function inherits( baseClass , nm)
    local name = nm
    local new_class = {}
    local class_table = { __index = new_class }

    function new_class:new(o)
        local o = o or {}
        setmetatable( o, class_table )
        return o
    end

    if nil ~= baseClass then
        setmetatable( new_class, { __index = baseClass } )
    end

    function new_class:get_name()
        return name
    end
   
    function new_class:super_class()
        return baseClass
    end

    return new_class
end
--Creating classes
Shape = inherits( nil, "Shape" )  

Circle = inherits( Shape, "Circle" )

Rectangle = inherits( Shape, "Rectangle")

Square = inherits( Rectangle, "Square" )

--instantiating classes
shap = Shape:new{filled = true, color = "red"}
circ = Circle:new()
rect = Rectangle:new()
squa = Square:new()

--showing the class hierarchy
print("   Function10   ")
print("-----------------")
print(" I am a " .. shap:get_name())
print(" I am a " .. circ:get_name() .. " and a " .. circ:super_class():get_name() )
print(" I am a " .. rect:get_name() .. " and a " .. rect:super_class():get_name() )
print(" I am a " .. squa:get_name() .. ", "      .. squa:super_class():get_name() .." and " .. squa:super_class():super_class():get_name() )



